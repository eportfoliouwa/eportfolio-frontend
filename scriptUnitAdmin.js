    var portfolioApp = angular.module('portfolioApp', ['ngRoute']);

    //define routes
    portfolioApp.config(function($routeProvider) {
    	$routeProvider

    	.when('/', {
    		templateUrl : 'pages/unitAdminHome.html',
    		controller : 'homeController'
    	})
    });

    //define controllers

    //main controller brings back all information about student
    //TODO: clean up competencies
    portfolioApp.controller('mainController', function($scope) {

    });
    
    portfolioApp.controller('homeController', function($scope) {
        
    });