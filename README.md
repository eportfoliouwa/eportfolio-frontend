# **ePortfolio** #

The ePortfolio is an online tool that allows students to maintain a portfolio of their practical experiences throughout the duration of their study. The ePortfolio has been created specifically for students undertaking the Master of Professional Engineering at the University of Western Australia who are required to demonstrate sixteen competencies specified by Engineers Australia. These competencies may be demonstrated through compulsory professional practicum placements, other practical experiences and coursework. The ePortfolio allows students to maintain a record of practical experiences and provides a text-editing tool for the production of reports in which competencies are mapped to practical experiences.   

## Version ##
 
1.0
## Application Features ##
### Dashboard-style Home Page ###
* Changes made to Practicum Entries and Career Episodes are reflected in the corresponding panels
* Progress tracking through Competencies Summary and Progress panels
* Export reports to PDF

![ePortfolioHomePage.jpg](https://bitbucket.org/repo/eaeq7o/images/680947013-ePortfolioHomePage.jpg)

### Practicum Entry Page ###
* Autocomplete form based on previous entries, validation and ability to add an attachment

![PracticumEntry.jpg](https://bitbucket.org/repo/eaeq7o/images/3099114644-PracticumEntry.jpg)

### Career Episode Page ###
* Easy to use text-editor with competency mapping functionality and word count
* Ability to add attachments

![CareerEpisode.jpg](https://bitbucket.org/repo/eaeq7o/images/3687299870-CareerEpisode.jpg) 
  
## A Quick Guide to the Repository ##

The ePortfolio repository is comprised of three main components:

### Backend ###
* Implemented using the Flask web framework for Python
* Provides a RESTful API for accessing portfolio resources with LTI standards based authentication from Learning Management Systems (LMSs).
* Uses the SQLAlchemy Object Relational Modelling modules to allow persistent storage with SQLite, MySQL or Postgres.
* S3 is used as the persistent storage location for all uploaded attachments, excluding static content files.

### API ###
* A RESTful API providing read/write access to the functionality of the backend.
* Used as a central point of reference from which both systems have been built
* Documentation can be accessed here <LINK FOR DOCS - TODO>

### Frontend ###
* Implementation uses HTML, CSS, Bootstrap and Angular (v1)
* Export to PDF functionality has been developed using the third party libraries jsPDF and jsPDF-autotable
* All dependencies are loaded using a CDN
* The ePorfolio is a single page application with each page injected into indexStudent.html. All other pages are located within the pages folder.
* The scriptStudent.js file defines the services, routes and controllers and the env.js file is used for the management of environment variables.   


### Want to know more? ###

* Contact the ePortfolio team through the CITS555X coordinator at the University of Western Australia.