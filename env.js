(function (window) {
  window.__env = window.__env || {};
  window.__env.apiUrl = 'https://test-api.engportfol.io';
  window.__env.baseUrl = '/';
  window.__env.enableDebug = true;
}(this));