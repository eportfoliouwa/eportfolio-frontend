# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from time import sleep
import xmlrunner

class Test7DeletePracticumCancel(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
	self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "http://localhost:8000/indexStudent.html"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_delete_practicum_cancel(self):
        driver = self.driver
        driver.get(self.base_url + "#/home/21742778")

	self.driver.add_cookie({'name': 'user_id', 'value': '21742778'})
        self.driver.add_cookie({'name': 'user_email', 'value': '21742778@student.uwa.edu.au'})
        self.driver.add_cookie({'name': 'first_name', 'value': 'Huiying'})
        self.driver.add_cookie({'name': 'last_name', 'value': 'Hu'})
        self.driver.add_cookie({'name': 'Role', 'value': 'student'})
        
        sleep(5)

        
        table = driver.find_element_by_id("practicumEntryTable")
        all_rows = table.find_elements_by_css_selector('tr')
        all_rows[-1].find_element_by_tag_name("a").click()
        sleep(5)

        driver.find_element_by_id("buttonDelete").click()
        sleep(2)
        
        driver.find_element_by_css_selector("button.btn.btn-default").click()
        sleep(2)
        
        driver.find_element_by_link_text("Cancel").click()
        sleep(2)

        driver.delete_all_cookies()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
