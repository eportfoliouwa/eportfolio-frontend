    var portfolioApp = angular.module('portfolioApp', ['ngRoute']);

    //define routes
    portfolioApp.config(function($routeProvider) {
    	$routeProvider

        .when('/', {
            templateUrl : 'pages/markerHome.html',
            controller : 'indexController'
        })

    	.when('/student', {
    		templateUrl : 'pages/markerStudentHome.html',
    		controller : 'homeController'
    	})

    	.when('/practicum', {
			templateUrl : 'pages/markerPracticum.html',
			controller : 'markerPracticumController'
    	})

    	.when('/episode', {
    		templateUrl : 'pages/markerCareerEpisode.html',
    		controller : 'markerEpisodeController'
    	})

        .when('/submission', {
            templateUrl : 'pages/markerSubmission.html',
            controller : 'markerSubmissionController'
        });
    });

    //define controllers

    //main controller brings back all information about student
    //TODO: clean up competencies
    portfolioApp.controller('mainController', function($scope) {
        $scope.student = {
            studentNumber: "20506654",
            practicumEntries: [
                {placementName:'IBM Perth', experienceType:'Workplace Practicum', hours: 20},
                {placementName:'UWA Robogals', experienceType:'Volunteer Practicum', hours: 10},
                {placementName:'Vix Technology', experienceType: 'WorkplacePracticum', hours: 40}
            ],
            careerEpisodes: [
                {episodeName: 'Career Episode 1', competencies: '1.1, 1.3'},
                {episodeName: 'Career Episode 2', competencies: '1.4, 1.7'}
            ],
            competencies: [
                {competency: '1.1', associatedEpisodes: ['1', '2']},
                {competency: '1.2', associatedEpisodes: ['1', '3']},
                {competency: '1.3', associatedEpisodes: ['4']},
                {competency: '1.4', associatedEpisodes: []},
                {competency: '1.5', associatedEpisodes: []},
                {competency: '1.6', associatedEpisodes: []},
                {competency: '2.1', associatedEpisodes: []},
                {competency: '2.2', associatedEpisodes: ['4','5']},
                {competency: '2.3', associatedEpisodes: []},
                {competency: '2.4', associatedEpisodes: []},
                {competency: '3.1', associatedEpisodes: []},
                {competency: '3.2', associatedEpisodes: []},
                {competency: '3.3', associatedEpisodes: []},
                {competency: '3.4', associatedEpisodes: []},
                {competency: '3.5', associatedEpisodes: []},
                {competency: '3.6', associatedEpisodes: []}
            ]

        };
    });

    portfolioApp.controller('indexController', function($scope) {

    });

    portfolioApp.controller('homeController', function($scope) {
        
    });

    //TODO: pass parameters into controller from url to determine which entry shows etc. 
    portfolioApp.controller('markerPracticumController', function($scope) {

        
    });

    portfolioApp.controller('markerEpisodeController', function($scope) {

    });

    portfolioApp.controller('markerSubmissionController', function($scope) {

    });