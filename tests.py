# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, os
from time import sleep
import xmlrunner
from unittest import TestLoader, TestSuite
from EportfolioFrontendTestCase.test1CreateSavePracticum import Test1CreateSavePracticum
from EportfolioFrontendTestCase.test2UpdatePracticum import Test2UpdatePracticum
from EportfolioFrontendTestCase.test3CreateSaveCE import Test3CreateSaveCE
from EportfolioFrontendTestCase.test4UpdateCE import Test4UpdateCE
from EportfolioFrontendTestCase.test5DeleteCECancel import Test5DeleteCECancel
from EportfolioFrontendTestCase.test6DeleteCEConfirm import Test6DeleteCEConfirm
from EportfolioFrontendTestCase.test7DeletePracticumCancel import Test7DeletePracticumCancel
from EportfolioFrontendTestCase.test8DeletePracticumConfirm import Test8DeletePracticumConfirm
from EportfolioFrontendTestCase.test9ExportAndSubmit import Test9ExportAndSubmit
from xmlrunner import xmlrunner

class EportfolioFrontendTestCase(unittest.TestCase):

        
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    suite = TestLoader()
    test_create_save_practicum = suite.loadTestsFromTestCase(Test1CreateSavePracticum)
    test_update_practicum = suite.loadTestsFromTestCase(Test2UpdatePracticum)
    test_create_save_ce = suite.loadTestsFromTestCase(Test3CreateSaveCE)
    test_update_ce = suite.loadTestsFromTestCase(Test4UpdateCE)
    test_delete_ce_cancel = suite.loadTestsFromTestCase(Test5DeleteCECancel)
    test_delete_ce_confirm = suite.loadTestsFromTestCase(Test6DeleteCEConfirm)
    test_delete_practicum_cancel = suite.loadTestsFromTestCase(Test7DeletePracticumCancel)
    test_delete_practicum_confirm = suite.loadTestsFromTestCase(Test8DeletePracticumConfirm)
    test_export_submit = suite.loadTestsFromTestCase(Test9ExportAndSubmit)
    
    suite = TestSuite([
        test_create_save_practicum,
        test_update_practicum,
        test_create_save_ce,
        test_update_ce,
        test_delete_ce_cancel,
        test_delete_ce_confirm,
        test_delete_practicum_cancel,
        test_delete_practicum_confirm,
        test_export_submit
                       ])
    unittest.runner = xmlrunner.XMLTestRunner(output=os.environ['CIRCLE_TEST_REPORTS'] + "/junit/test-results/")
    result = unittest.runner.run(suite)
    if result.wasSuccessful():
        exit()
    else:
        exit(1)
