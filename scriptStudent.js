var env = {};
if(window) {
    Object.assign(env,window.__env);
}

function ApiService(__env){
    this.apiUrl = __env.apiUrl;
    return this.apiUrl;
}

function disableLogging($logProvider, __env){  
  $logProvider.debugEnabled(__env.enableDebug);
}

disableLogging.$inject = ['$logProvider', '__env'];

ApiService.$inject =[ '__env'];

angular.module('portfolioApp', ['ngRoute', 'ngCookies'])
    
    .constant('__env', env)
    .config(disableLogging) 
    .service('api', ApiService(env))
    
    // fileupload directive
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }])

    //define services
    .factory('serviceLibrary', ['$http', '$cookies', '$route', function($http, $cookies, $route) {
        var services = {
            getStudent: function(){
                var id = $cookies.get('user_id');
                return $http.get(ApiService(env)+'/v1/eportfolios/'+id,{withCredentials:true}).success(function(data) {
                    return data;
                });
            },
            getFileTypes: function() {
                return $http.get(ApiService(env)+'/v1/types/file',{withCredentials:true}).success(function(data) {
                    return data;
                })
            },
            getCompetencies: function(){
                return $http.get(ApiService(env)+'/v1/competencies',{withCredentials:true}).success(function(data) {
                    return data;
                })
            },
            getPracticumEntry: function(studentId,entryId){
                return $http.get(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryId,{withCredentials:true}).success(function(data) {
                    return data;
                })
            },
            getCareerEpisode: function(studentId,entryId,episodeId){
                return $http.get(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryId+'/episodes/'+episodeId,{withCredentials:true}).success(function(data) {
                    return data;
                })
            },
            getPractiumTypes: function() {
                return $http.get(ApiService(env)+'/v1/types/entry',{withCredentials:true}).success(function(data) {
                    return data;
                })
            },
            getCommonFiles: function() {
                return $http.get(ApiService(env)+'/v1/files',{withCredentials:true}).success(function(data) {
                    return data;
                })
            }
        }
        return services;
    }])

    //define routes
    .config(function($routeProvider) {
    	$routeProvider
        .when('/home/:id', {
            templateUrl : 'pages/studentHome.html',
            controller : 'homeController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                competencies: function(serviceLibrary) {
                    return serviceLibrary.getCompetencies();
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                },
                commonFiles: function(serviceLibrary) {
                    return serviceLibrary.getCommonFiles();
                }
            }
        })
        .when('/practicum', {
            templateUrl : 'pages/studentPracticum.html',
            controller : 'studentPracticumController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                practicumEntry: function(serviceLibrary) {
                    return null;
                },
                practicumTypes: function(serviceLibrary) {
                    return serviceLibrary.getPractiumTypes();
                },
                isNew: function() {
                    return true;
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                }
            }
        })   
        .when('/episode/:entryId', {
            templateUrl : 'pages/studentCareerEpisode.html',
            controller : 'studentEpisodeController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                competencies: function(serviceLibrary,$route) {
                    return serviceLibrary.getCompetencies();
                },
                careerEpisode: function(serviceLibrary,$route) {
                    return null;
                },
                isNew: function() {
                    return true;
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                },
                practicumEntry: function(serviceLibrary) {
                    return null;
                }
            }
        })
        .when('/practicum/:studentId/:entryId', {
            templateUrl : 'pages/studentPracticum.html',
            controller : 'studentPracticumController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                practicumEntry: function(serviceLibrary,$route) {
                    var entryId = $route.current.params.entryId;
                    var studentId = $route.current.params.studentId;
                    return serviceLibrary.getPracticumEntry(studentId,entryId)
                },
                practicumTypes: function(serviceLibrary) {
                    return serviceLibrary.getPractiumTypes();
                },
                isNew: function() {
                    return false;
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                }
            }
        })
        .when('/episode/:studentId/:entryId/:episodeId', {
            templateUrl : 'pages/studentCareerEpisode.html',
            controller : 'studentEpisodeController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                competencies: function(serviceLibrary,$route) {
                    return serviceLibrary.getCompetencies();
                },
                careerEpisode: function(serviceLibrary,$route) {
                    var episodeId = $route.current.params.episodeId;
                    var entryId = $route.current.params.entryId;
                    var studentId = $route.current.params.studentId;
                    return serviceLibrary.getCareerEpisode(studentId, entryId, episodeId);
                },
                isNew: function() {
                    return false;
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                },
                practicumEntry: function(serviceLibrary,$route) {
                    var entryId = $route.current.params.entryId;
                    var studentId = $route.current.params.studentId;
                    return serviceLibrary.getPracticumEntry(studentId,entryId)
                },
            }
        })     
        .when('/submission', {
            templateUrl : 'pages/studentSubmission.html',
            controller : 'studentSubmissionController',
            resolve: {
                student: function(serviceLibrary) {
                    return serviceLibrary.getStudent();
                },
                filetypes: function(serviceLibrary) {
                    return serviceLibrary.getFileTypes();
                }
            }
        });
    })

    //define controllers
    .controller('mainController', function($scope) {

    })

    //homepage 
    .controller('homeController', ['$scope', 'student', 'commonFiles', 'competencies', '$http', 'filetypes', function($scope, student, commonFiles, competencies, $http, filetypes) 
    {
        var user = student.data;
        var studentId = student.data.data.portfolioStudent.userId;
        var competencies = competencies.data; 
        var careerEpisodes = [];
        var ce = new Object();

        $scope.hours = user.data.portfolioCat1HoursCompleted;
        $scope.hoursPercent = user.data.portfolioCat1HoursCompletedPercent;
        $scope.totalHours = user.data.portfolioHoursCompleted;
        $scope.totalHoursPercent = user.data.portfolioHoursCompletedPercent;
        $scope.competenciesCompleted = user.data.portfolioCompetenciesCompleted;
        $scope.competenciesCompletedPercent = user.data.portfolioCompetenciesCompletedPercent; 
        $scope.helpUrl = "";
        $scope.eaUrl = "";

        $.each(commonFiles.data, function()
        {
            if($(this)[0].fileType.indexOf("EngineersAustraliaCompetencies") > -1)
            {
                $scope.eaUrl = ApiService(env) + $(this)[0].fileURL;
            }
            if($(this)[0].fileType.indexOf("HelpPages") > -1)
            {
                $scope.helpUrl = ApiService(env) + $(this)[0].fileURL;
            }
        });
        $.each(user.data.portfolioEntries, function(i)
        {
            var entry = $(this)[0];
            $.each($(this)[0].entryCareerEpisodes, function(j)
            {   
                careerEpisodes.push({
                    key: $(this)[0],
                    value: entry
                });
            });
        })

        var mapping = [];
        $.each(careerEpisodes, function()
        {
            ce = $(this)[0];
            mapping[ce.key.id] = []
            if(ce.key.episodeSelected)
            {
                $.each(competencies.data, function()
                {
                    var c = $(this)[0].competencyNumber;
                    var found = ce.key.episodeCompetencies.filter(function(index){
                        return index.competencyNumber.indexOf(c) > -1
                    });
                    if(found.length > 0)
                    {
                        mapping[ce.key.id][c] = true;
                    }
                    else
                    {
                        mapping[ce.key.id][c] = false;
                    }
                });
            }
            
        });

        $('#uploadCV').on('click', function() {
            $('#cvModal').modal({show:true});
        });

        function uploadFileToUrl(file, url)
        {
            var fd = new FormData();
            fd.append('file', file);
            $http.put(url, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                withCredentials: true,
            }).success(function(){
                location.reload();
            });
        }

        $http.get(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',{withCredentials:true}).success(function(data){
            $.each(data.data, function()
            {
                if($(this)[0].attachmentFileType == filetypes.data[0].fileType)
                {
                    $scope.resumeLink = $(this)[0].attachmentFileURL;
                    $scope.resumeName = $(this)[0].attachmentFileName;
                }
            });
        });       

        $('#uploaded').on('click', function() {
            $http.get(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',{withCredentials:true}).success(function(data){
                $.each(data.data, function()
                {
                    if($(this)[0].attachmentFileType == filetypes.data[0].fileType)
                    {
                        var id = $(this)[0].id;
                        $http.delete(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments/'+id, {withCredentials:true}).success(function(data){
                        });
                    }     
                });       
            });
            var fileName = $('#attachment').val().split('/').pop().split('\\').pop();
            var dataObj = {
                data: {
                    'attachmentFileName': fileName,
                    'attachmentFileType': filetypes.data[0].fileType
                    }
                }
            $http.post(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',dataObj,{withCredentials:true}).success(function(data, status, headers, config) {                
                var url = headers()['location'];
                var file = $scope.myFile;//document.getElementById('attachment').files[0];
                uploadFileToUrl(file, headers()['location']);
            });
        });

        $('#exportCV').on('click', function() {
            if($scope.hoursPercent < 100 ||
                $scope.totalHoursPercent < 100 ||
                $scope.competenciesCompletedPercent < 100 ||
                $scope.resumeLink == "" ||
                $scope.careerEpisodes.length < 3 ||
                $scope.careerEpisodes.length > 5)
                {
                   $('#failedSubmitModal').modal({show:true}); 
                }
            var doc = new jsPDF('p', 'pt');
            var pageHeight = doc.internal.pageSize.height;

            $.each(careerEpisodes, function()
            {
                var reportHtml = $(this)[0].key.episodeReport;
                $('#hiddenExport').append(reportHtml);
                 var columns = ["Competency", "Report"];
                 var rows = [];

                 var editorTable = $(reportHtml).filter('#editorTable')[0]

                $.each(($(editorTable).find('tr')), function()
                {
                    var text = "";

                    if($(this).find('.text-editor').text() != null)
                    {
                        text = $(this).find('.text-editor').text();
                    }

                    var competency = convertClassToCompetency($(this).attr('class'));

                    $('#hiddenExport').show();
                    $('#hiddenExport').hide();
                    rows.push([competency, text]);

                });

                var episodeName = $(this)[0].key.episodeName;
                doc.autoTable(columns, rows, {
                    startY: doc.autoTableEndPosY() + 60,
                    beforePageContent: function(data) {
                        doc.text(episodeName, 40, doc.autoTableEndPosY() -10);
                    },
                    margin: { bottom : 40},
                    styles: {
                        
                        overflow: 'linebreak',
                        pageBreak: 'auto'
                    },
                    columnStyles: {

                    }
                });
            });
            doc.save('eportfolio'+studentId+'.pdf');
        });

        $scope.competencies = competencies.data.sort(function(a,b){return(a.competencyNumber-b.competencyNumber)});
        $scope.careerEpisodes = careerEpisodes;
        $scope.student = user.data.portfolioStudent;   
        $scope.mapping = mapping;
        $scope.entries = user.data.portfolioEntries;
    }])

    //practicum entry
    .controller('studentPracticumController', ['$scope', '$filter','student', '$routeParams', 'practicumEntry', 'practicumTypes', 'isNew', '$http', 'filetypes', function($scope, $filter, student, $routeParams, practicumEntry, practicumTypes, isNew, $http, filetypes) {
        var careerEpisodes = [];
        var user = student.data;
        var entryNumber = $routeParams.entryId; 
        var studentId = student.data.data.portfolioStudent.userId;
        $scope.studentId = studentId;
        $scope.formData = {};
        $.each(user.data.portfolioEntries, function()
        {
            $.each($(this)[0].entryCareerEpisodes, function(j)
            {   
                careerEpisodes.push($(this)[0]);
            });
        });

        $scope.practicumTypes = [];
        $.each(practicumTypes.data, function() {
            $scope.practicumTypes.push($(this)[0].entryType);
        });
        
        if(entryNumber != null)
        {
            $scope.formData.entryName = practicumEntry.data.data.entryName;
            if(practicumEntry.data.data.entryStartDate != null)
            {
                $scope.formData.entryStartDate = new Date(practicumEntry.data.data.entryStartDate);
            }
            if(practicumEntry.data.data.entryEndDate != null)
            {
                $scope.formData.entryEndDate = new Date(practicumEntry.data.data.entryEndDate);
            }

            $scope.formData.entryDescription = practicumEntry.data.data.entryDescription;
            $scope.formData.entryType = practicumEntry.data.data.entryType;
            $scope.formData.entrySupervisorName = practicumEntry.data.data.entrySupervisorName;
            $scope.formData.entrySupervisorPosition = practicumEntry.data.data.entrySupervisorPosition;
            $scope.formData.entryCareerEpisodes = practicumEntry.data.data.entryCareerEpisodes;
            $scope.entryPracticumEntry = practicumEntry;
            $scope.formData.entryHoursAccrued = practicumEntry.data.data.entryHoursAccrued;
            if(practicumEntry.data.data.entryAttachments[0] != null)
            {
                $scope.existingFileName = practicumEntry.data.data.entryAttachments[0].attachmentFileName;
                $scope.existingFileLink = practicumEntry.data.data.entryAttachments[0].attachmentFileURL;
            }
        }

        $scope.$watch('formData.entryType', function(newVal, oldVal){
            if($scope.formData.entryType == "Coursework")
            {
                $('#hours').attr('disabled', 'disabled');
                $scope.formData.entryHoursAccrued = 0;
            }
            else if (entryNumber != null)
            {   
                $('#hours').removeAttr('disabled');
                $scope.formData.entryHoursAccrued = practicumEntry.data.data.entryHoursAccrued;
            }
            else
            {
                $('#hours').removeAttr('disabled');
                $scope.formData.entryHoursAccrued = null;
            }
        });

        $scope.careerEpisodes = careerEpisodes;
        
        if(!isNew)
        {
            $('#buttonDelete').show();
        }

        $('#buttonSave').on('click', function()
        {
            if($scope.formData.entryName == null || 
                $scope.formData.entryDescription == null ||
                $scope.formData.entryStartDate == null ||
                $scope.formData.entryType == null ||
                $scope.formData.entryHoursAccrued == null ||
                $scope.formData.entrySupervisorName == null ||
                $scope.formData.entrySupervisorPosition == null ||
                isNaN($scope.formData.entryHoursAccrued) == true ||
                parseInt($scope.formData.entryHoursAccrued) < 0) 
            {
                $('#validationModal').modal({show:true});
            }
            else
            {
                if($scope.myFile!= null)
                {
                    var attachmentDetails = null;
                    var fileName = $('#attachment').val().split('/').pop().split('\\').pop();
                    var fileDataObj = {
                        data: {
                            'attachmentFileName': fileName,
                            'attachmentFileType': filetypes.data[0].fileType
                        }
                    }
                    $http.post(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',fileDataObj,{withCredentials:true}).success(function(data, status, headers, config) {                
                        var url = headers()['location'];
                        var file = $scope.myFile;
                        var fd = new FormData();
                        fd.append('file', file);
                        $http.put(url, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined},
                            withCredentials: true
                        }).success(function() {
                            $http.get(url,{withCredentials:true}).success(function(data) {
                                $scope.formData.entryAttachments = [{
                                    "id": data.data.id,
                                    "attachmentUrl": data.data.attachmentFileURL,
                                    "attachmentFileType": filetypes.data[1].fileType}
                                ];
                                var dataObj = {data: $scope.formData};
                                if(isNew == true)
                                {
                                    saveUrl = ApiService(env)+'/v1/eportfolios/'+studentId+'/entries'; 
                                    $http.post(saveUrl,dataObj,{withCredentials:true}).success(function(data) {
                                        $('#saveModal').modal({show:true});
                                        $('#practicumSave').on('click',function()
                                        {
                                            window.location.href = "#/home/"+studentId;
                                        })
                                    });
                                }
                                else
                                {
                                    $('#buttonDelete').show();
                                    saveUrl = ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber;
                                    $http.put(saveUrl,dataObj,{withCredentials:true}).success(function(data) {
                                        $('#saveModal').modal({show:true});
                                        $('#practicumSave').on('click',function()
                                        {
                                            window.location.href = "#/home/"+studentId;
                                        })
                                    });   
                                }
                            });
                        })
                    });
                }
                else
                {
                    var dataObj = {data: $scope.formData};
                    if(isNew == true)
                    {
                        saveUrl = ApiService(env)+'/v1/eportfolios/'+studentId+'/entries'; 
                        $http.post(saveUrl,dataObj,{withCredentials:true}).success(function(data) {
                            $('#saveModal').modal({show:true});
                            $('#practicumSave').on('click',function()
                            {
                                window.location.href = "#/home/"+studentId;
                            })
                        });
                    }
                    else
                    {
                        $('#buttonDelete').show();
                        saveUrl = ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber;
                        $http.put(saveUrl,dataObj,{withCredentials:true}).success(function(data) {
                            $('#saveModal').modal({show:true});
                            $('#practicumSave').on('click',function()
                            {
                                window.location.href = "#/home/"+studentId;
                            })
                        });   
                    }
                }  
            } 
        });


        $('#practicumDelete').on('click', function() {
            $http.delete(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber).success(function(data) {
                window.location.href = "#/home/"+studentId;
            });
        });              
    }])

    //career episode
    .controller('studentEpisodeController', ['$scope', 'competencies', '$compile', 'careerEpisode', 'isNew', 'student', '$routeParams', '$http', 'filetypes', 'practicumEntry', function($scope, competencies, $compile, careerEpisode, isNew, student, $routeParams, $http, filetypes, practicumEntry) {
        var newRow = null;
        var row = null;
        var count = 0;
        var competencies = competencies.data.data;
        var competency = 0;
        var split = [];
        var className = "";
        var selected = "";
        var words = 0;
        var text = "";
        var savedText = "";
        var studentId = student.data.data.portfolioStudent.userId;
        var entryNumber = $routeParams.entryId;
        $scope.selectedCompetencies = [];
        $scope.tickedCompetencies = [];
        $scope.studentId = studentId;
        $scope.practicumNumber = $routeParams.entryId;
        
        $scope.practicumName = "";
        if(practicumEntry != null)
        {
            $scope.practicumName = practicumEntry.data.data.entryName;
        }
        
        function checkCompetencies() {
            $scope.selectedCompetencies = [];
            $scope.tickedCompetencies = [];
            $.each($('#editorTable').find('tr'), function(){
                if($(this)[0].className!="")
                {
                    var number = convertClassToCompetency($(this).attr('class'));
                    var contained = false;
                    var competencyObject = competencies.filter(function(index){
                            return index.competencyNumber.indexOf(number) > -1
                    });
                    $.each($scope.selectedCompetencies, function() {

                        if($(this)[0].competencyNumber.indexOf(number) > -1)
                        {
                            contained = true;
                        }
                    });
                    if(!contained)
                    {
                         $scope.tickedCompetencies.push(number);
                         $scope.selectedCompetencies.push(competencyObject[0]);
                    }                     
                }
            });
        }

        if(!isNew)
        {
            var episode = careerEpisode.data.data;
            var report = episode.episodeReport;
            var episodeNumber = episode.episodeNumber;
            $scope.episodeName = episode.episodeName;
            $scope.careerEpisode = episode;
            $scope.isSelected = episode.episodeSelected;
            document.getElementById('reportBody').innerHTML = report;
            $compile($('#editorTable').contents())($scope);
            checkCompetencies();
        }
        else
        {
            $scope.episodeName = "";
        }
        
        $scope.competencies = competencies;
        $scope.competencies.unshift({competencyNumber:"None"});        

        if(!isNew)
        {
            $('#buttonDelete').show();
        }

        $('#add').on('click', function() {
            count = $('#editorTable').find('tr').length + 1;
            newRow = $('<tr id="row-' + count + '"><td><span class="badge" id="badge'+count+'"></span></td><td style="vertical-align: middle; padding-left: 1%; background-color:white; width:100%;">' +
            '<div class="text-editor" contenteditable="true" style="text-align: left; font-size:16px; word-break: break-all;"><span></span></div></td><td style="padding:1%; width:10%; vertical-align:top"><select class="competencies" ng-model="selectedCompetency' + count + '"' +
            'ng-options="c.competencyNumber + \' \' + c.competencyName for c in competencies" ng-change="update()"><option value="" disabled>Select competency</option></select>' +
            '</td><td style="vertical-align: top; padding:1%; width:10%"><button class="btn btn-primary btn-xs remove" ng-click="onRemove($event)">Remove' +
            '</button></td></tr>');
            $('#editorTable tbody').append(newRow);

            $compile($('#editorTable').contents())($scope);            
        });

        $scope.update = function()
        {
            id = row.attr('id').split('-')[1];
            selected = 'selectedCompetency' + id;    
            competency = $scope[selected];
            className = competency.competencyNumber;
            if(competency.competencyNumber.toString().indexOf('None') <= -1)
            {
                split = competency.competencyNumber.toString().split('.');
                className = '_' + split[0] + '-' + split[1];
            }
            row.removeClass();
            row.addClass(className);
            if(competency.competencyNumber != 'None')
            {
                $('#badge'+id).text(competency.competencyNumber);
            }
            else
            {

                $('#badge'+id).text('');
            }
            checkCompetencies();           
        }

        $('#editorTable tbody').on('click', '.competencies', function() {
            row = $(this).parents("tr");
        });

        //$('#editorTable tbody').on('click', '.remove', function() {
        $scope.onRemove = function(event)
        {
            row = $(event.target).parent().parent()
            row.remove();
            checkCompetencies();
        }
            

        //});

        $('#editorTable').on('input', '.text-editor', function () {
            size = $('.text-editor').length;
            text = $('.text-editor').text();
            words = text.trim().replace(/\s+/g, ' ').split(' ').length + size - 1;
            $('#wordCount').text('Word Count: ' + words);
        });

        $('#saveModal').on('click', function() {
            window.location.href = "#/home/"+studentId;
        });
        $('#buttonSave').on('click', function() {
            if($('#episodeName').val() == "")
            {
                $('#validationModal').modal({show:true});
            }
            else
            {
                var markupToSave = $('#reportBody').html();
                var selected = false;
                if($('#selectedEpisode').is(':checked'))
                {
                    selected = true;
                }
                
                if($scope.myFile!= null)
                {
                    var attachmentDetails = null;
                    var fileName = $('#attachment').val().split('/').pop().split('\\').pop();
                    var fileDataObj = {
                        data: {
                            'attachmentFileName': fileName,
                            //TODO: make call to get enums
                            'attachmentFileType': filetypes.data[2].fileType
                        }
                    }
                    $http.post(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',fileDataObj,{withCredentials:true}).success(function(data, status, headers, config) {                
                        var url = headers()['location'];
                        var file = $scope.myFile;
                        var fd = new FormData();
                        fd.append('file', file);
                        $http.put(url, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined},
                            withCredentials: true
                        }).success(function() {
                            $http.get(url,{withCredentials:true}).success(function(data) {
                                var attachmentsArray = [];
                                if(!isNew)
                                {
                                    attachmentsArray = episode.episodeAttachments;
                                }
                                attachmentsArray.push({
                                    "id": data.data.id,
                                    "attachmentUrl": data.data.attachmentFileURL,
                                    "attachmentFileType": filetypes.data[1].fileType
                                });
                                var dataObj = {
                                data: {
                                    'episodeLastUpdated': new Date($.now()),
                                    'episodeAttachments': attachmentsArray,
                                    'episodeCompetencies': $scope.selectedCompetencies,
                                    'episodeSelected': selected,
                                    'episodeReport': markupToSave,
                                    'episodeName': $('#episodeName').val()
                                    }
                                }
                                if(isNew)
                                {
                                    $http.post(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber+'/episodes',dataObj,{withCredentials:true}).success(function(data) {
                                        isNew = false; 
                                        $('#buttonDelete').show();                                            
                                        $('#saveModal').modal({show:true});

                                    });
                                }
                                if(!isNew)
                                {
                                    $http.put(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber+'/episodes/'+episodeNumber,dataObj,{withCredentials:true}).success(function(data) {
                                        $('#saveModal').modal({show:true});
                                        $.each($('.deleteCheckbox'), function() {
                                            if($(this).is(":checked"))
                                            {
                                                id = $(this).attr('id')
                                                $http.delete(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments/'+id, {withCredentials:true}).success(function(data) {
                                                });
                                            }
                                        })
                                        $('#saveModal').modal({show:true});
                                    });
                                }
                            });   
                        });
                    });
                }
                else
                {
                    var dataObj = {
                    data: {
                        'episodeLastUpdated': new Date($.now()),
                        'episodeCompetencies': $scope.selectedCompetencies,
                        'episodeSelected': selected,
                        'episodeReport': markupToSave,
                        'episodeName': $('#episodeName').val()
                        }
                    }
                    if(isNew)
                    {
                        $http.post(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber+'/episodes',dataObj,{withCredentials:true}).success(function(data) {
                            $('#saveModal').modal({show:true});
                            isNew = false; 
                            $('#buttonDelete').show();
                        });
                    }
                    if(!isNew)
                    {
                        $http.put(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber+'/episodes/'+episodeNumber,dataObj,{withCredentials:true}).success(function(data) {
                            $.each($('.deleteCheckbox'), function() {
                                if($(this).is(":checked"))
                                {
                                    id = $(this).attr('id')
                                    $http.delete(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments/'+id, {withCredentials:true}).success(function(data) {
                                    });
                                }
                            })
                            $('#saveModal').modal({show:true});
                        });
                    }
                }
            }            
        });


        $('#episodeDelete').on('click', function() {
            if(!isNew)
            {
                $http.delete(ApiService(env)+'/v1/eportfolios/'+studentId+'/entries/'+entryNumber+'/episodes/'+episodeNumber, {withCredentials:true}).success(function(data) {
                    window.location.href = "#/home/"+studentId;
                });
            }
        });
        
    }])

    //submission
    .controller('studentSubmissionController', ['$scope', 'student', '$http', 'filetypes', function($scope, student, $http, filetypes) {
        var user = student.data;
        var studentId = student.data.data.portfolioStudent.userId;
        var careerEpisodes = [];
        var portfolioEntries = user.data.portfolioEntries;
        
        $.each(user.data.portfolioEntries, function(i)
        {
            var entry = $(this)[0];
            $.each($(this)[0].entryCareerEpisodes, function(j)
            {   
                if($(this)[0].episodeSelected == true)
                {
                    careerEpisodes.push({
                    key: $(this)[0],
                    value: entry
                    });
                }     
            });
        })

        $scope.studentId = studentId; 
        $scope.careerEpisodes = careerEpisodes;
        $scope.hours = user.data.portfolioCat1HoursCompleted;
        $scope.hoursPercent = user.data.portfolioCat1HoursCompletedPercent;
        $scope.totalHours = user.data.portfolioHoursCompleted;
        $scope.totalHoursPercent = user.data.portfolioHoursCompletedPercent;
        $scope.competenciesCompleted = user.data.portfolioCompetenciesCompleted;
        $scope.competenciesCompletedPercent = user.data.portfolioCompetenciesCompletedPercent;    
        $scope.resumeLink = "";
        $http.get(ApiService(env)+'/v1/eportfolios/'+studentId+'/attachments',{withCredentials:true}).success(function(data){
            $.each(data.data, function()
            {
                if($(this)[0].attachmentFileType == filetypes.data[0].fileType)
                {
                    $scope.resumeLink = $(this)[0].attachmentFileURL;
                    $scope.resumeName = $(this)[0].attachmentFileName;
                }
            });
        });

        $scope.workCertificates = true;
        $.each(user.data.portfolioEntries, function(i)
        {
            if($(this)[0].entryAttachments.length <= 0)
            {
                $scope.workCertificates = false;
                return false;
            }

        });
        var portfolioEntries = user.data.portfolioEntries;
        $scope.check = function()
        {

            if($scope.hoursPercent < 100 ||
                $scope.totalHoursPercent < 100 ||
                $scope.competenciesCompletedPercent < 100 ||
                $scope.resumeLink == "" ||
                $scope.careerEpisodes.length < 3 ||
                $scope.careerEpisodes.length > 5 ||
                $scope.workCertificates == false)
                {
                   $('#failedSubmitModal').modal({show:true});   
                }
                else
                {
                    $('#successfulSubmitModal').modal({show:true});
                    var doc = new jsPDF('p', 'pt');
                    var pageHeight = doc.internal.pageSize.height;
                    
                    $.each(portfolioEntries, function()
                    {
                        var columns = ["",""];
                        var rows = [];
                        var entryName = $(this)[0].entryName;
                        rows.push(["Placement Name", $(this)[0].entryName]);
                        rows.push(["Placement Start", $(this)[0].entryStartDate]);
                        rows.push(["Placement End", $(this)[0].entryEndDate]);
                        rows.push(["Placement Description", $(this)[0].entryDescription]);
                        rows.push(["Placement Type", $(this)[0].entryType]);
                        rows.push(["Hours", $(this)[0].entryHoursAccrued]);
                        rows.push(["Supervisor Name", $(this)[0].entrySupervisorName]);
                        rows.push(["Supervisor Position", $(this)[0].entrySupervisorPosition]);

                        doc.autoTable(columns, rows, {
                            startY: doc.autoTableEndPosY() + 60,
                            beforePageContent: function(data) {
                                doc.text("Practicum Entry: " + entryName, 40, doc.autoTableEndPosY() -10);
                            },
                            margin: { bottom : 40},
                            styles: {
                                
                                overflow: 'linebreak',
                                pageBreak: 'auto'
                            },
                            columnStyles: {

                            }
                        });

                        $.each($(this)[0].entryCareerEpisodes, function(j)
                        {   
                            if($(this)[0].episodeSelected == true)
                            {
                                var reportHtml = $(this)[0].episodeReport;
                                $('#hiddenExport').append(reportHtml);
                                 var columns = ["Competency", "Report"];
                                 var rows = [];

                                 var editorTable = $(reportHtml).filter('#editorTable')[0]

                                $.each(($(editorTable).find('tr')), function()
                                {
                                    var text = "";
                                    if($(this).find('.text-editor').text() != null)
                                    {
                                        text = $(this).find('.text-editor').text();
                                    }
                                    var competency = convertClassToCompetency($(this).attr('class'));
                                    $('#hiddenExport').show();
                                    $('#hiddenExport').hide();
                                    rows.push([competency, text]);
                                });
                                var episodeName = $(this)[0].episodeName;
                                doc.autoTable(columns, rows, {
                                    startY: doc.autoTableEndPosY() + 60,
                                    beforePageContent: function(data) {
                                        doc.text("Career Episode: " + episodeName, 40, doc.autoTableEndPosY() -10);
                                    },
                                    margin: { bottom : 40},
                                    styles: {
                                        
                                        overflow: 'linebreak',
                                        pageBreak: 'auto'
                                    },
                                    columnStyles: {

                                    }
                                });
                            }
                        });

                    })
                    doc.save('eportfolio'+studentId+'.pdf'); 

                }
            }
    }]);

    function convertClassToCompetency(className)
    {
        if(className != null)
        {
            var number = className.replace(/_/,"");
            number = number.replace(/-/,"."); 
            return number; 
        }
    }


